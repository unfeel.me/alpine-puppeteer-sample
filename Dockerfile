FROM node:current-alpine

RUN apk add --update \
    chromium

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD 1
ENV PUPPETEER_EXECUTABLE_PATH /usr/bin/chromium-browser

WORKDIR /riza

CMD ["node", "/riza/run.js"]