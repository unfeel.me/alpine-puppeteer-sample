# Alpine Docker Puppeteer

puppeteer based on docker-alpine


### build:
```
cd app;
npm install;

docker build -t kopet .
```

### run:
```
docker run --rm -ti -v $PWD/app:/riza/ kopet
```
