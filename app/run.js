/**
versi 1

@author riza
*/
const puppeteer = require('puppeteer')

;(async () => {
    
    console.log('Start Kopet!')

    var width = 600
    var height = 600
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--auto-open-devtools-for-tabs',
            '--window-size=' + width + ',' + height
        ]
    })
    
    const page = await browser.newPage()
    await page.setViewport({ width: 1000, height: 1200 })
    await page.setUserAgent(
        'Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0'
    )
    var url = 'https://pinterest.com'
    await page.goto(url, {
        waitUntil: 'networkidle0'
    });

    var t = await page.title()
    console.log(t)

    await browser.close();

})()
